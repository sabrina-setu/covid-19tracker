import 'package:covid_tracker/sign_in.dart';
import 'package:covid_tracker/sign_up.dart';
import 'package:flutter/material.dart';

class SignInUp extends StatefulWidget {
  static const routeName = '/signInUp_screen';
  @override
  _SignInUpPageState createState() => _SignInUpPageState();
}

class _SignInUpPageState extends State<SignInUp> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: new Scaffold(
        //backgroundColor: Color(0xffFFF8F6 ),
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(60),
          child: Padding(
            padding: const EdgeInsets.only(top: 10),
            child: new AppBar(
              elevation: 0,
              flexibleSpace: new Column(
                children: [
                  new Container(
                    // margin: EdgeInsets.only(top: 15),
                    // child: Row(
                    //   mainAxisAlignment: MainAxisAlignment.center,
                    //   children: [
                    //     // Padding(
                    //     //   padding: const EdgeInsets.only(left: 25),
                         
                    //     // ),
                    //     // Expanded(
                    //     //   child: Row(
                    //     //     mainAxisAlignment: MainAxisAlignment.center,
                    //     //     children: [

                    //     //     ],
                    //     //   ),
                    //     // )
                    //   ],
                    // ),
                  ),
                  new SizedBox(height: 10,),
                  new TabBar(
                    isScrollable: true,
                    indicatorWeight: 0.01,
                    labelColor: Colors.white,
                    labelStyle: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 22

                    ),
                    unselectedLabelStyle: TextStyle(

                    ),

                    tabs: <Widget>[
                      Tab(
                        text: "Sign In",
                      ),Tab(
                        text: "Sign Up",
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
        body: TabBarView(
            children: [
              Container(
                height: 400,
                child: SignIn(),
              ),
              Container(
                height: 700,
                  child: SignUp(),
                
              ),
            ]),

      ),
    );
  }
}