
import 'package:flutter/material.dart';	
enum SingingCharacter { male, female, other }
//Statefull Wideget
class SignUp extends StatefulWidget {
    @override	 
    _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUp> {
bool _showPassword = false;
 SingingCharacter _character = SingingCharacter.male;

         @override	  
                Widget build(BuildContext context) {
                  return Scaffold(
    body: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(height:15),
                           Center(
                        child: Container(
                          width: 330,
                          height: 680,
                          color: Color.fromRGBO
                          (98, 102, 114, 0.1),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children:<Widget> [
                               SizedBox(height:20),
                              Padding(
                                padding: const EdgeInsets.only(left:10.0),
                                child: Text('Personal',textAlign: TextAlign.left, style: TextStyle(
        color: Colors.white,
        fontSize: 24,
        letterSpacing: 0,
        fontWeight: FontWeight.bold,
        height: 1
      ),),
                              ),
                              
              
                              SizedBox(height:20),
                              Padding(

                                padding: const EdgeInsets.all(15.0),
                             child: TextField(    
autocorrect: true,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintText: 'First Name',
              hintStyle: TextStyle(color: Color.fromRGBO(169, 176, 181, 0.41)),
              filled: true,
              fillColor: Colors.black,
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
              ),
            ),
    //hintText: 'User Name'
),),
SizedBox(height:10),
  Padding(
                                padding: const EdgeInsets.all(15.0),
                             child: TextField(    
autocorrect: true,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintText: 'Last Name',
              hintStyle: TextStyle(color: Color.fromRGBO(169, 176, 181, 0.41)),
              filled: true,
              fillColor: Colors.black,
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
              ),
            ),
    //hintText: 'User Name'
),),
SizedBox(height:10),
  Padding(
                                padding: const EdgeInsets.all(15.0),
                             child: TextField(    
autocorrect: true,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintText: 'Phone No',
              hintStyle: TextStyle(color: Color.fromRGBO(169, 176, 181, 0.41)),
              filled: true,
              fillColor: Colors.black,
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
              ),
            ),
    //hintText: 'User Name'
),),
SizedBox(height:10),
  Padding(
                                padding: const EdgeInsets.all(15.0),
                             child: TextField(    
autocorrect: true,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintText: 'Age',
              hintStyle: TextStyle(color: Color.fromRGBO(169, 176, 181, 0.41)),
              filled: true,
              fillColor: Colors.black,
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
              ),
            ),
    //hintText: 'User Name'
),),
 RadioListTile<SingingCharacter>(
          title: const Text('Male'),
          value: SingingCharacter.male,
          groupValue: _character,
          onChanged: (SingingCharacter value) {
            setState(() {
              _character = value;
            });
          },
        ),
         RadioListTile<SingingCharacter>(
          title: const Text('Female'),
          value: SingingCharacter.female,
          groupValue: _character,
          onChanged: (SingingCharacter value) {
            setState(() {
              _character = value;
            });
          },
        ),
         RadioListTile<SingingCharacter>(
          title: const Text('Other'),
          value: SingingCharacter.other,
          groupValue: _character,
          onChanged: (SingingCharacter value) {
            setState(() {
              _character = value;
            });
          },
        ),

                            ],
                          ),
                        ),
                      ),
                     SizedBox(height: 40),
                      Center(
                        child: Container(
                          width: 330,
                          height: 250,
                          color: Color.fromRGBO
                          (98, 102, 114, 0.1),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children:<Widget> [
              
                              SizedBox(height:20),
                              Padding(

                                padding: const EdgeInsets.all(15.0),
                             child: TextField(    
                               obscureText: !this._showPassword,                
               autocorrect: false,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintText: 'Password',
              hintStyle: TextStyle(color: Color.fromRGBO(169, 176, 181, 0.41)),
            suffixIcon: IconButton(
          icon: Icon(
           this._showPassword ? Icons.visibility : Icons.visibility_off,
           color: Color(0xff008e7b),
          ),
          onPressed: () {
            setState(() => this._showPassword = !this._showPassword);
          },
        ),
              filled: true,
              fillColor: Colors.black,
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
              ),
            ),
    //hintText: 'User Name'
),),
SizedBox(height:10),
  Padding(
                                padding: const EdgeInsets.all(15.0),
                             child: TextField(    
                               obscureText: !this._showPassword,                
               autocorrect: false,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintText: 'Confirm Password',
              hintStyle: TextStyle(color: Color.fromRGBO(169, 176, 181, 0.41)),
            suffixIcon: IconButton(
          icon: Icon(
           this._showPassword ? Icons.visibility : Icons.visibility_off,
           color: Color(0xff008e7b),
          ),
          onPressed: () {
            setState(() => this._showPassword = !this._showPassword);
          },
        ),
              filled: true,
              fillColor: Colors.black,
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
              ),
            ),
    //hintText: 'User Name'
),),

                            ],
                          ),
                        ),
                      ),
                  SizedBox(height: 40),
                        ButtonTheme(
                minWidth: 268,
                height: 55,
                buttonColor : Color(0xff008e7b),  
              child:  RaisedButton(
            shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(50.0))),
                onPressed: () => {},                             
            child: const Text('Sign Up', textAlign: TextAlign.center, style: TextStyle(
        color: Colors.white,
        fontSize: 16,
        letterSpacing: 0,
        fontWeight: FontWeight.bold,
        height: 1
      ),)
          ),
            ) ,
            SizedBox(height: 20),
                  ],
                  ),
                    )
                  );
                }	
}


