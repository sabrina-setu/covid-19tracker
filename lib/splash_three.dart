import 'package:covid_tracker/sign_InUp.dart';
import 'package:flutter/material.dart';	


//Statefull Wideget
class SplashScreenThree extends StatefulWidget {
  static const routeName = '/splashThree_screen';
    @override	 
    _SplashScreenThreePageState createState() => _SplashScreenThreePageState();
}

class _SplashScreenThreePageState extends State<SplashScreenThree> {
         @override	  
                Widget build(BuildContext context) {
                  return Scaffold(
                    body: Stack(
                      fit: StackFit.expand,
                      children: <Widget>[
                        Padding(padding: const EdgeInsets.all(10.0)),
                       Column(
                         mainAxisAlignment: MainAxisAlignment.center,
                       children: <Widget>[
                        Image(
              image: AssetImage('assets/Ripple Vector.png')
            ),
            Padding(padding: const EdgeInsets.all(20),
            child: Text('Allow Location', textAlign: TextAlign.center, style: TextStyle(
                      fontSize: 20,
        letterSpacing: 0,
        fontWeight: FontWeight.bold,
        height: 1
            ),),            
            ),
            Padding(padding: const EdgeInsets.all(20),
            child: Text('Please allow location after tapping on Detect Location below. Its required to move forward',textAlign: TextAlign.center, style: TextStyle(
                      fontSize: 16,
        letterSpacing: 0,
        fontWeight: FontWeight.normal,
        height: 1
            ),),            
            ),
                        ButtonTheme(
                minWidth: 268,
                height: 55,
                buttonColor : Color(0xff008e7b),  
              child:  RaisedButton(
            shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(50.0))),
                onPressed: () => { Navigator.of(context).pushNamed(SignInUp.routeName),
                                       },                             
            child: const Text('Detect Location', textAlign: TextAlign.center, style: TextStyle(
        color: Colors.white,
        fontSize: 16,
        letterSpacing: 0,
        fontWeight: FontWeight.bold,
        height: 1
      ),)
          ),
            ) 
                         
                       ],) 
                      ]
                    )
                  );
                }	
}


