import 'package:covid_tracker/splash_three.dart';
import 'package:flutter/material.dart';
import 'package:introduction_screen/introduction_screen.dart';


//Statefull Wideget
class SplashScreenTwo extends StatefulWidget {
  static const routeName = '/splashTwo_screen';
    @override	 
    _SplashScreenTwoPageState createState() => _SplashScreenTwoPageState();
}

class _SplashScreenTwoPageState extends State<SplashScreenTwo> {
  List<PageViewModel>getPage(){
    return [PageViewModel(
        image: Center(
    child:                         Image(
              image: AssetImage('assets/Map Vector.png')
            ),
  ),
       title: "Share Your Location",
  body: "We need your location to monitor status and well being of your family and communnity around",
    decoration: const PageDecoration(
    titleTextStyle: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 22),
    bodyTextStyle: TextStyle(fontWeight: FontWeight.normal, fontSize: 16.0,color: Colors.white,),
  ),
    ),
        PageViewModel(
        image: Center(
    child:                         Image(
              image: AssetImage('assets/Family Vector.png')
            ),
  ),
       title: "Add Your Family",
  body: "Add your family and other members living with you at your place and update status of everyone",
      decoration: const PageDecoration(
    titleTextStyle: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 22),
    bodyTextStyle: TextStyle(fontWeight: FontWeight.normal, fontSize: 16.0,color: Colors.white,),
  ),
    ),
        PageViewModel(
        image: Center(
    child:                         Image(
              image: AssetImage('assets/App Vector.png')
            ),
  ),
       title: "Share Your Location",
  body: "Keep app updated always as we add new features to better take care of your family and community",
      decoration: const PageDecoration(
    titleTextStyle: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 22),
    bodyTextStyle: TextStyle(fontWeight: FontWeight.normal, fontSize: 16.0,color: Colors.white,),
  ),
    ),
    ];
  }
         @override	  
                Widget build(BuildContext context) {
                  return Scaffold(
                    body: 
                        IntroductionScreen(
  pages: getPage(),
  done: const Text("Done", style: TextStyle(fontWeight: FontWeight.w600)),
  onDone: () {
    Navigator.of(context).pushNamed(SplashScreenThree.routeName);
  },
   dotsDecorator: DotsDecorator(
    size: const Size.square(10.0),
    activeSize: const Size(50.0, 10.0),
    activeColor:Color(0xff008e7b),
    color: Colors.black26,
    spacing: const EdgeInsets.symmetric(horizontal: 3.0),
    activeShape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(25.0)
    )
  ),
  ),          
                  );
                }	
}


