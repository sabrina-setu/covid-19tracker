
import 'package:covid_tracker/screens/home_page.dart';
import 'package:covid_tracker/screens/loading_screen.dart';
import 'package:covid_tracker/screens/summery.dart';
import 'package:covid_tracker/sign_InUp.dart';
import 'package:covid_tracker/sign_in.dart';
import 'package:covid_tracker/sign_up.dart';
import 'package:covid_tracker/splash_three.dart';
import 'package:covid_tracker/splash_one.dart';
import 'package:covid_tracker/splash_three.dart';
import 'package:covid_tracker/splash_two.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

//screen
import './screens/root_page.dart';
import './screens/details_screen.dart';
import './screens/view_all_screen.dart';
import './screens/link_details_screen.dart';

//providers
import './providers/home_provider.dart';


void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp,DeviceOrientation.portraitDown]);
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: Color(0xff008e7b),
    statusBarIconBrightness: Brightness.dark
  ));
  runApp(MyApp());
  }


class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
     return MultiProvider(
      providers: [
        ChangeNotifierProvider<HomeProvider>(
          create: (BuildContext ctx)=>HomeProvider(),
        )
      ],
    child: MaterialApp(
       theme: ThemeData(
            brightness: Brightness.dark,
          ),
      // initialRoute: '/',
      // routes: {
      //   '/': (context) => SplashScreenOne(),
      //   '/splashScreenTwo':(context) =>SplashScreenTwo(),
      //   '/splashScreenThree':(context)=>SplashScreenThree(),
      //   '/signInUp': (context) => SignInUp(),
      //   '/creditScreen':(context) =>CreditScreen(),
      //   '/detailsScreen':(context) =>DetailsScreen(),
      //   '/homePage':(context) =>HomePage(),
      //   '/linkDetailsScreen':(context) =>LinkDetailsScreen(),
      //   '/loadingScreen':(context) =>LoadingScreen(),
      //   '/rootPage':(context) =>RootPage(),
      //   '/summery':(context) =>Summery(),
      //   '/viewAll':(context) =>ViewAll(),

      // },
      home: SplashScreenOne(),
        routes: {
          SplashScreenOne.routeName : (BuildContext ct)=>SplashScreenOne(),
          SplashScreenTwo.routeName : (BuildContext ct)=>SplashScreenTwo(),
          SplashScreenThree.routeName : (BuildContext ct)=>SplashScreenThree(),
          SignInUp.routeName : (BuildContext ct)=>SignInUp(),
          RootPage.routeName : (BuildContext ct)=>RootPage(),
          DetailsScreen.routeName : (BuildContext ct)=>DetailsScreen(),
          ViewAll.routeName :(BuildContext ct)=>ViewAll(),
          LinkDetailsScreen.routeName : (BuildContext ct)=>LinkDetailsScreen()
        },
      //home: SplashScreenOne(),
      title: 'Covid-19 Tracker',
    ),
     );
  }
}



